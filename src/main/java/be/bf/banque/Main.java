package be.bf.banque;

import be.bf.banque.models.CompteCourant;
import be.bf.banque.models.Titulaire;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Titulaire titulaire = new Titulaire();
        titulaire.nom = "Ovyn";
        titulaire.prenom = "Flavian";
        titulaire.dateNaissance = LocalDate.of(1991, 7, 19);

        CompteCourant courant = new CompteCourant();
        courant.numero = "BE12 1234 1234 1234";
        courant.titulaire = titulaire;
        courant.ligneCredit = 250;
    }
}
