package be.bf.banque.models;

/**
 * Classe mutable représentant un compte pouvant aller en négatif
 * FA: CompteCourant{numero, solde, ligneCredit}
 *
 * @attribute numero String => Format IBAN BEXX XXXX XXXX XXXX
 * @attribute solde double
 * @attribute ligneCredit double
 * @attribute titulaire Titulaire
 *
 * @invariant numero != null && numero.length = 19
 * @invariant solde >= -ligneCredit
 * @invariant ligneCredit >= 0
 * @invariant titulaire != null
 */
public class CompteCourant {
    public String numero;
    public double solde;
    public double ligneCredit;
    public Titulaire titulaire;

    /**
     * Méthode permettant d'ajouter un montant au solde du compte
     * @param montant > 0
     * @modify this.solde | this.solde = this.solde + montant
     */
    public void depot(double montant) {
        if (montant <= 0) return;
        this.solde += montant;
    }

    /**
     * Méthode permettant de soustraitre un montnat du solde
     * @param montant > 0
     * @modify this.solde | this.solde = this.solde - montant ssi this.solde - montant >= -this.ligneCredit
     */
    public void retrait(double montant) {
        if (montant <= 0) return;
        if (this.solde - montant < -this.ligneCredit) return;

        this.solde -= montant;
    }
}
