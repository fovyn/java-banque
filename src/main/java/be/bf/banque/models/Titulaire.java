package be.bf.banque.models;

import java.time.LocalDate;

/**
 * Class mutable représentant un titulaire de compte au sein de la banque
 * FA: Titulaire{nom, prenom}
 *
 * @attribute nom String
 * @attribute prenom String
 * @attribute dateNaissance LocalDate
 *
 * @invariant nom != null && nom.length > 0
 * @invariant prenom != null && prenom.length > 0
 */
public class Titulaire {
    public String nom;
    public String prenom;
    public LocalDate dateNaissance;
}
